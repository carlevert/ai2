package ai.ass2;

/**
 * Mood-recognizing "computer" with trainable perceptrons.
 * 
 * The computer is a an array of perceptrons, one perceptron for each mood.
 * Indexes of the perceptron array (0..3) maps to four moods (1..4).
 * 
 * @author Carl-Evert Kangas, Stina Olofsson
 * 
 */
public class Computer {

    private static int NUM_PERCEPTRONS = 4;

    private Perceptron[] perceptrons;

    /**
     * Constructs a new Computer-object with NUM_PERCEPTRONS perceptrons.
     */
    public Computer() {
	perceptrons = new Perceptron[NUM_PERCEPTRONS];
	for (int i = 0; i < NUM_PERCEPTRONS; i++)
	    perceptrons[i] = new Perceptron(Face.IMAGE_HEIGHT
		    * Face.IMAGE_WIDTH);
    }

    /**
     * Tells each perceptron to recognize (or not) a face as having a certain
     * mood.
     * 
     * @param face
     *            A face
     * @param mood
     *            Integer 1, 2, 3 or 4 for happy, sad, mischievous and mad
     *            respectively.
     */
    public void learnFace(Face face, int mood) {
	for (int i = 0; i < NUM_PERCEPTRONS; i++)
	    perceptrons[i].learn(face.getPixels(), ((mood - 1) == i));
    }

    /**
     * Sums all perceptrons current error for a face.
     * 
     * @param face
     *            A face
     * @param mood
     *            Integer 1, 2, 3 or 4 for happy, sad, mischievous or mad.
     * @return The sum of all perceptrons error to their desired outputs.
     */
    public double getTotalErrorForFace(Face face, int mood) {
	double totalError = 0.0;
	for (int i = 0; i < NUM_PERCEPTRONS; i++) {
	    double error = perceptrons[i].getAbsoluteError(face.getPixels(),
		    (mood - 1) == i);

	    totalError += error;
	}
	return totalError;
    }

    /**
     * Returns the mood classification which is the index + 1 of the perceptron
     * with the highest output.
     * 
     * @param face
     *            A face
     * @return Integer 1, 2, 3 or 4 for happy, sad, mischievous and mad
     *         respectively.
     */
    public int classifyFace(Face face) {
	int highestPerceptronIndex = 0;
	double highest = perceptrons[0].outputFor(face.getPixels());

	for (int i = 1; i < NUM_PERCEPTRONS; i++) {
	    double tmp = perceptrons[i].outputFor(face.getPixels());
	    if (tmp >= highest)
		highestPerceptronIndex = i;
	}
	return highestPerceptronIndex + 1;
    }

}
