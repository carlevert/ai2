package ai.ass2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * This class is the operator that operates the perceptron computer. That is,
 * trains the system of perceptrons until recognition is thrustworthy and then
 * does classification on a set of unknown faces.
 * 
 * @author Carl-Evert Kangas, Stina Olofsson
 */
public class Operator {

    /* Faces used to train the network */
    private ArrayList<Face> trainingFaces;

    /* Faces used to test if network performance */
    private ArrayList<Face> testFaces;

    /*
     * Set of faces to classify after acceptable performance level has been
     * reached.
     */
    private ArrayList<Face> checkFaces;

    /* Facit, used when training and evaluating performance */
    private HashMap<Integer, Integer> trainingFacit;

    private Computer computer;

    /* Target error per face in performance test */
    private static final double TARGET_ERROR = 0.88;

    /*
     * Defines how many faces in the training set that should be used for
     * training and performance testing.
     */
    private static final double TRAINING_PART = 0.85;

    public Operator() {
	trainingFaces = new ArrayList<>();
	testFaces = new ArrayList<>();
	checkFaces = new ArrayList<>();
	computer = new Computer();
    }

    /**
     * Trains the network with faces in trainingFaces.
     */
    public void train() {
	double error;
	int numTestFaces = testFaces.size();
	
	do {
	    /* Shuffle the faces each training round */
	    Collections.shuffle(trainingFaces);

	    /* Learn the computer to recognize face with mood from facit */
	    for (Face face : trainingFaces)
		computer.learnFace(face, trainingFacit.get(face.getId()));

	    /* Check if acceptable performance has been reached */
	    error = 0.0;
	    for (Face face : testFaces)
		error += computer.getTotalErrorForFace(face,
			trainingFacit.get(face.getId()));
	} while (error > TARGET_ERROR * numTestFaces);
    }

    /**
     * Let the trained computer do classification on the set of faces in
     * checkFaces. Output is printed on standard out on the format ImageX Y
     * where X is the face id and y is the mood.
     */
    public void doClassification() {
	for (Face face : checkFaces) {
	    int classification = computer.classifyFace(face);
	    System.out.println("Image" + face.getId() + " " + classification);
	}
    }

    /**
     * Parse the training file and split it into lists trainingFaces and
     * testFaces for training and testing performance respectively.
     * 
     * @param trainingFacesFilename
     */
    public void readTrainingFaces(String trainingFacesFilename) {
	ArrayList<Face> allFaces = Parser.readFaceFile(trainingFacesFilename);

	int numTrainingFaces = (int) (allFaces.size() * TRAINING_PART);

	int i = 0;
	for (; i < numTrainingFaces; i++)
	    trainingFaces.add(allFaces.get(i));
	for (; i < allFaces.size(); i++)
	    testFaces.add(allFaces.get(i));
    }

    /**
     * Parse the training facit.
     * 
     * @param trainingFacitFilename
     */
    public void readTrainingFacit(String trainingFacitFilename) {
	trainingFacit = Parser.readFacitFile(trainingFacitFilename);
    }

    /**
     * Parse the checkFacesFilename, i.e. the faces that should be classified.
     * 
     * @param checkFacesFilename
     */
    public void readCheckFaces(String checkFacesFilename) {
	checkFaces = Parser.readFaceFile(checkFacesFilename);
    }

}
