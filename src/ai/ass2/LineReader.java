package ai.ass2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Wraps away some Java-verboseness, also provides line numbering for the last
 * line read.
 * 
 * @author Carl-Evert Kangas, Stina Olofsson
 */
public class LineReader extends BufferedReader {

    private int lineNo = 0;

    public LineReader(Reader reader) {
	super(reader);
    }

    public LineReader(String filename) throws FileNotFoundException {
	super(new FileReader(new File(filename)));
    }

    @Override
    public String readLine() throws IOException {
	lineNo++;
	return super.readLine();
    }

    public int getLineNo() {
	return lineNo;
    }

}
