package ai.ass2;

/**
 * Class used for storing Face-data such as pixel values and face ids.
 * 
 * @author Carl-Evert Kangas, Stina Olofsson
 */
public class Face {

    public static final int IMAGE_WIDTH = 20;
    public static final int IMAGE_HEIGHT = 20;
    public static final int IMAGE_LEVELS = 32;
    
    private int id;
    private double[] pixels;
    private int cursor;
    private int numPixels;

    /**
     * Constructs a new Face-object
     */
    public Face() {
	id = 0;
	cursor = 0;
	numPixels = IMAGE_HEIGHT * IMAGE_WIDTH;
	pixels = new double[numPixels];
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    /**
     * @return the pixel array
     */
    public double[] getPixels() {
	return pixels;
    }

    /**
     * Stores a pixel in an array. The value is converted to a double 0.0-1.0.
     * 
     * @param value
     *            Next pixel value
     */
    public void putPixel(byte value) {
	pixels[cursor++] = ((double) value) / (IMAGE_LEVELS - 1);
    }

    /**
     * A face is not complete and valid without id and before all pixels are
     * read.
     * 
     * @return true if valid
     */
    public boolean isValid() {
	return id != 0 && cursor == numPixels;
    }

}
