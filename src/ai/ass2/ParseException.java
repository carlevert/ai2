package ai.ass2;

/**
 * Provides means for doing consistent error messages when parsing files.
 * 
 * @author Carl-Evert Kangas, Stina Olofsson
 */
@SuppressWarnings("serial")
public class ParseException extends Exception {

	public ParseException(String message, int line, String filename) {
		super(message + " (" + filename + ":" + line + ")");
	}

	public ParseException(String message) {
		super(message);
	}

}
