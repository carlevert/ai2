package ai.ass2;

/**
 * Trainable perceptron implemented according to the algorithm on slides from
 * the ANN lecture on Fundamentals of Artificial Intelligence.
 * 
 * @author Carl-Evert Kangas, Stina Olofsson
 * 
 */
public class Perceptron {
    /**
     * Learning factor
     */
    public static final double ALPHA = 0.007;

    /**
     * Constants used in this class. If changed, the activationFunction needs to
     * be adjusted accordingly.
     */
    public static final double DESIRED_OUTPUT = 1.0;
    public static final double UNDESIRED_OUTPUT = 0.0;

    private double weights[];

    /**
     * Initializes perceptron with random weights.
     */
    public Perceptron(int inputLength) {
	weights = new double[inputLength + 1];
	for (int i = 0; i < weights.length; i++)
	    weights[i] = Math.random();
    }

    /**
     * Adjusts perceptrons weights up if the output of the perceptron matches
     * the desired output or down when the perceptron misfired.
     * 
     * @param input
     *            , array of doubles
     * @param desiredOutput
     *            , whether this input should be recognized or not
     */
    public void learn(double[] input, boolean desiredOutput) {
	double error = getError(input, desiredOutput);
	updateWeights(input, error);
    }

    /**
     * Computes the output for the perceptron for the given input. The output is
     * the dot product of the weight vector and the input vector, taken through
     * an activation function which in this case is a sigmoid function.
     * 
     * The last weight of the weight vector is used for shifting the sigmoid to
     * the "left" or "right" and does not have a corresponding component in the
     * input vector.
     * 
     * @param input
     *            the input vector
     * @return Values close to 0 when not recognizing input, close to otherwise.
     */
    public double outputFor(double[] input) {
	double sum = 0.0;
	for (int i = 0; i < input.length; i++)
	    sum += input[i] * weights[i];
	sum += 1.0 * weights[weights.length - 1];
	return activationFunction(sum);
    }

    /**
     * Computes the error between perceptrons output and the desired output.
     * 
     * @param input
     *            the input vector
     * @param desiredOutput
     *            , whether this input should be recognized or not
     * @return error. This error is negative when desiredOutput == false
     */
    private double getError(double[] input, boolean desiredOutput) {
	double a = outputFor(input);
	double error = desiredOutput ? DESIRED_OUTPUT - a : UNDESIRED_OUTPUT
		- a;
	return error;
    }

    public double getAbsoluteError(double[] input, boolean desiredOutput) {
	return Math.abs(getError(input, desiredOutput));
    }

    /**
     * Updates the weight vector of this perceptron according to the input and
     * error.
     * 
     * @param input
     *            , the input vector
     * @param error
     *            , difference between the desired and actual output
     */
    private void updateWeights(double[] input, double error) {
	for (int i = 0; i < input.length; i++)
	    weights[i] += ALPHA * error * input[i];
	weights[weights.length - 1] += ALPHA * error * 1.0;
    }

    /**
     * A sigmoid function for keeping output within 0.0 .. 1.0 range. The
     * functions result is magically shifted sideways by adding the last weight
     * of the weight vector to the exponent in the denominator.
     * 
     * @param sum
     *            dot product of weights and inputs, including extra weight and
     *            the additional imaginary input.
     * @return Values close to 1 when input should be recognized, close to 0
     *         otherwise
     */
    private double activationFunction(double sum) {
	double lastWeight = weights[weights.length - 1];
	double ret = 1.0 / (1 + Math.exp(-1 * sum + lastWeight));
	return ret;
    }

}
