package ai.ass2;

/**
 * Main class for the Faces application.
 * 
 * @author Carl-Evert Kangas, Stina Olofsson
 */
public class Faces {

    public static void main(String[] args) {
	if (args.length == 3) {
	    /* Some variable names for clearness. */
	    String trainingFacesFilename = args[0];
	    String trainingFacitFilenam = args[1];
	    String checkFacesFilename = args[2];

	    /* Functionality of Faces application is
	     * run by an Operator-object.
	     */
	    Operator operator = new Operator();

	    /* Read files needed for performing the face recognition task */
	    operator.readTrainingFaces(trainingFacesFilename);
	    operator.readTrainingFacit(trainingFacitFilenam);
	    operator.readCheckFaces(checkFacesFilename);

	    /* Train and classify, output is written to standard out */
	    operator.train();
	    operator.doClassification();

	} else {
	    System.err.println("Usage: java Faces TRAINING FACIT TEST");
	    System.exit(1);
	}

    }
}
