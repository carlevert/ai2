package ai.ass2;

import java.io.FileNotFoundException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses face definition files and facit files for the Neural Network
 * assignment on. Files are parsed line-by-line with regular expressions. Some
 * error checking is provided by the regular expressions, some is provided by
 * the target class Face that keeps track of numbers of pixels read and some
 * error checking is simply done in the logic of this class.
 * 
 * Full specification of the face and facit-files can be found at
 * https://www8.cs.umu.se/kurser/5DV121/HT15/assignment2/faces.html.
 * 
 * @author Carl-Evert Kangas, Stina Olofsson
 *
 */
public class Parser {

    /* Some patterns for recognizing parts of face-files and facit lines */
    private static final String imagedataPattern = "^\\d{1,2}( \\d{1,2}){"
	    + (Face.IMAGE_WIDTH - 1) + "}\\s*$";
    private static final String headingPattern = "^Image(\\d+)\\s*$";
    private static final String blankPattern = "^\\s*$";
    private static final String commentPattern = "^#.*";
    private static final String facitPattern = "^Image(\\d+)\\s+([1234])\\s*$";

    /**
     * Reads a facit file line by line, returning a hashmap with face id as key
     * and mood as value
     * 
     * @param filename
     *            to be parsed
     * @return a facit hashmap
     */
    public static HashMap<Integer, Integer> readFacitFile(String filename) {

	HashMap<Integer, Integer> map = new HashMap<>();
	String line = null;
	Pattern pattern = Pattern.compile(facitPattern);
	Matcher matcher;

	try (LineReader reader = getLineReader(filename)) {

	    while ((line = reader.readLine()) != null) {

		/* Don't process comments or blank lines. */
		if (line.matches(blankPattern) || line.matches(commentPattern))
		    continue;

		/*
		 * Put values to map if line matches. If line cannot be parsed
		 * it syntactically not correct and a ParseError will be thrown
		 * causing program to exit.
		 */
		matcher = pattern.matcher(line);
		if (matcher.matches()) {
		    String key = matcher.group(1);
		    String value = matcher.group(2);
		    map.put(Integer.parseInt(key), Integer.parseInt(value));
		} else {
		    throw new ParseException("Syntax error",
			    reader.getLineNo(), filename);
		}

	    }

	} catch (IOException e) {
	    e.printStackTrace();
	} catch (ParseException e) {
	    System.err.println(e.getMessage());
	    System.exit(1);
	}

	return map;
    }

    /**
     * Reads a face-file line by line and create a Face-object for each face
     * definition. Return results in an ArrayList.
     * 
     * @param filename
     *            to be parsed
     * @return ArrayList of Face-objects in filename
     */
    public static ArrayList<Face> readFaceFile(String filename) {
	ArrayList<Face> faces = new ArrayList<>();

	try (LineReader reader = getLineReader(filename)) {
	    String line = null;
	    Face face = null;

	    while ((line = reader.readLine()) != null) {

		/* Image data part of image block */
		if (line.matches(imagedataPattern)) {
		    String[] levels = line.split("\\s+");
		    for (String level : levels) {
			try {
			    byte levelByte = Byte.parseByte(level);
			    if (levelByte >= 0
				    && levelByte < Face.IMAGE_LEVELS)
				face.putPixel(levelByte);
			    else
				throw new ParseException(
					"Image level out of bounds",
					reader.getLineNo(), filename);
			} catch (NumberFormatException e) {
			    throw new ParseException("NumberFormatException",
				    reader.getLineNo(), filename);
			}
		    }
		}

		/*
		 * Grab the ID-number from lines starting with "Image". This
		 * also indicates the start of an image block.
		 */
		if (line.matches(headingPattern)) {
		    if (face == null) {
			face = new Face();
			String idStr = line.replaceAll(headingPattern, "$1");
			int id = Integer.parseInt(idStr);
			face.setId(id);

		    } else {
			throw new ParseException(
				"Unexpected image block heading-line",
				reader.getLineNo(), filename);
		    }
		    continue;
		}

		/*
		 * When a blank line occurs: If there is a valid face, add it to
		 * the collection and continue reading.
		 */
		if (line.matches(blankPattern)) {
		    if (face != null && face.isValid())
			faces.add(face);
		    face = null;
		    continue;
		}

		/* Don't process comment-lines. */
		if (line.matches(commentPattern))
		    continue;

	    }

	    /*
	     * Finished looping through file but last line was not a blank line
	     */
	    if (face != null && face.isValid())
		faces.add(face);

	    // System.out.println("Number of faces parsed: " + faces.size());

	} catch (IOException e) {
	    e.printStackTrace();
	} catch (ParseException e) {
	    System.err.println(e.getMessage());
	    System.exit(1);
	}

	return faces;
    }

    /**
     * Returns a LineReader object or exits on error.
     * 
     * @param filename
     *            The text file to be read
     * @return LineReader object for filename
     */
    public static LineReader getLineReader(String filename) {
	LineReader lineReader = null;
	try {
	    lineReader = new LineReader(filename);
	} catch (FileNotFoundException e) {
	    System.err.println(filename + ": No such file");
	    System.exit(1);
	}
	return lineReader;
    }

}
